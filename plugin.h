#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Architecture/GUIElementBase/guielementbase.h"

#include "../../Interfaces/Utility/iprojectmanager.h"
#include "../../Interfaces/Utility/i_dev_plugin_info_data_extention.h"
#include "../../Interfaces/Utility/i_dev_project_manager_data_extention.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.ProjectManagerView" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	~Plugin() override;

private:
	GUIElementBase* m_guiElementBase;
	ReferenceInstancePtr<IProjectManager> m_projectManager;
	ReferenceInstancePtr<IDevPluginInfoDataExtention> m_devPluginInfo;
	ReferenceInstancePtr<IDevProjectManagerDataExtention> m_devProjectManager;
};
